<?php

namespace Domain\Maintenance\Repository;

use Domain\Maintenance\Exceptions\MaintenanceException;

class MaintenanceRepository {

    /**
     * @var MaintenanceStorage|null
     */
    private $_storage = null;

    /**
     * MaintenanceRepository constructor.
     *
     * @param MaintenanceStorage $_storage
     */
    public function __construct(MaintenanceStorage $_storage)
    {
        $this->_storage = $_storage;
    }

    /**
     * @param $request
     *
     * @return |null |null
     */
    public function store($request)
    {
        $data = $this->_storage->store($request);
        if (empty($data)){
            return null;
        }
    }

    /**
     * @param $request
     *
     * @return |null
     */
    public function show($request)
    {
        $data = $this->_storage->show($request);
        if (empty($data)){
            return null;
        }
        return $data;
    }

    /**
     * @param $request
     *
     * @return void|null
     */
    public function update($request)
    {
        $data = $this->_storage->update($request);
        if (empty($data)){
            return null;
        }
        return $data;
    }

    /**
     * @param $id
     *
     * @return |null
     */
    public function destroy($id)
    {
        $data = $this->_storage->destroy($id);
        if (empty($data)){
            return null;
        }
        return $data;
    }
}
