<?php

namespace Domain\Maintenance\Repository;

use App\Models\Maintenance;
use Carbon\Carbon;

class MaintenanceStorage {

    private $_model = null;

    public function __construct(Maintenance $model) {
        $this->_model = $model;
    }

    public function store($data)
    {
        $maintenance = new $this->_model;
        $maintenance->mileage = $data['mileage'];
        $maintenance->maintenance_info = $data['maintenance_info'];
        $maintenance->maintenance_date = $data['maintenance_date'];
        $maintenance->vehicle_id = $data['_vehicle_id'];
        $maintenance->save();
    }

    public function update($data)
    {
        $maintenance = $this->findById($data['_maintenance_id']);
        $maintenance->mileage = $data['mileage'];
        $maintenance->maintenance_info = $data['maintenance_info'];
        $maintenance->maintenance_date = $data['maintenance_date'];
        $maintenance->vehicle_id = $data['_vehicle_id'];
        $maintenance->save();
    }

    public function show($id)
    {
       return $this->findById($id);
    }


    public function destroy($id)
    {
        return $this->findById($id)->delete();
    }

    public function findById(int $id)
    {
        return $this->_model->find($id);
    }

}
