<?php

namespace Domain\Maintenance;

use Domain\Maintenance\Exceptions\MaintenanceException;
use Domain\Maintenance\Repository\MaintenanceRepository;
use Domain\Maintenance\Validations\MaintenanceValidation;

class MaintenanceService {

    private $_repository;

    public function __construct( MaintenanceRepository $repository )
    {
        $this->_repository = $repository;
    }

    /**
     * @param MaintenanceValidation $request
     *
     * @return mixed
     * @throws MaintenanceException
     */
    public function store( MaintenanceValidation $request )
    {
        try {
            return $this->_repository->store( $request );
        } catch ( \TypeError | \Exception $e ) {
            throw new MaintenanceException();
        }
    }

    /**
     * @param $id
     *
     * @return |null
     * @throws MaintenanceException
     */
    public function show($id)
    {
        try {
            return $this->_repository->show( $id );
        } catch ( \TypeError | \Exception $e ) {
            dd($e);
            throw new MaintenanceException();
        }
    }

    /**
     * @param MaintenanceValidation $request
     *
     * @return mixed
     * @throws MaintenanceException
     */
    public function update(MaintenanceValidation $request )
    {
        try {
            return $this->_repository->update( $request );
        } catch ( \TypeError | \Exception $e ) {
            throw new MaintenanceException();
        }
    }

    public function destroy(int $id)
    {
        $this->_repository->destroy($id);

        return back()->with('alert-success', 'Zapis je uspesno obrisan.');
    }

}
