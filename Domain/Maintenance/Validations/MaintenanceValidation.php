<?php

namespace Domain\Maintenance\Validations;

use Illuminate\Foundation\Http\FormRequest;

class MaintenanceValidation extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'mileage' => 'required|integer',
            'maintenance_date' => 'required',
            'maintenance_info' => 'required|min:3|max:1000',
            '_vehicle_id' => 'required',

        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'mileage.required' => 'Unesite kilometrazu!',
            'maintenance_date.required' => 'Unesite datum!',
            'maintenance_info.required' => 'Unesite opis!',
        ];
    }
}
